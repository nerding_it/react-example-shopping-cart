import React, { createRef, useRef } from 'react';
import { selectProducts, wish, unwish } from './productSlice';
import { useSelector, useDispatch } from 'react-redux';

import styles from './Product.module.css';

const ProductList = () => {
  const dispatch = useDispatch();
  const products = useSelector(selectProducts);

  const onload = async (event) => {
    const width = document.body.offsetWidth / 12,
	  height = 128;
    
    const { target: image } = event;
    const { previousSibling: canvas } = image;
    canvas.width = width;
    canvas.height = height;
    const context = canvas.getContext('2d');

    const bitmap = await createImageBitmap(
      image, 
      { resizeWidth: width, resizeHeight: height, resizeQuality: 'high' }
    );
    
    context.drawImage(bitmap, 0, 0);
    const offset = Math.floor(document.body.offsetWidth / 60);
    canvas.parentElement.animate(      
      Array.from(new Array(60), (_, i) => {
	if (i === 0) {
	  return { transform: `translateX(${offset * i}px)`}
	}
	return { transform: `translateX(-${offset * i}px)`}
      }),
      {
      duration: 500,
      iterations: 1
    })
  }
  
  return (
    <ol className={styles.listGrid}>
      {
	products.map((product, i) => {
	  return (
	    <li className={styles.listItem} key={product.id}>
	      <canvas></canvas>
	      <img onLoad={onload} className={styles.image} alt="Product" src={product.image} />	      
	      <small> { product.name } </small>
	      <em> { product.price } </em>
	      { product?.wished === true ?
		<button onClick={() => dispatch(unwish(product.id))} type="button">Remove </button>
		:
		<button onClick={() => dispatch(wish(product.id))} type="button"> Add </button>
	      }		
	    </li>
	  )
	})
      }
    </ol>
  )
}

export default ProductList;
