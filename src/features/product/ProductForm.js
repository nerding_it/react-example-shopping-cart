import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { add, selectCount } from './productSlice';
import styles from './Product.module.css';

const ProductForm = () => {
  const count = useSelector(selectCount);
  const dispatch = useDispatch();

  // Local state to manage current image url
  const [ image, setImage ] = useState(null);
  const [ name, setName ] = useState(null);
  const [ price, setPrice ] = useState(null);
  const [ error, setError ] = useState(null);
  
  const submit = (event) => {
    event.preventDefault();
    const payload = { name, price, image };
    // Make sure name is starting with valid alphabet
    if (!name.match(/^[a-zA-Z][a-zA-Z0-9\s]+/)) {
      setError('Please enter valid name');
      return;
    }
    if (!Number.isFinite(price) || Math.sign(price) === -1) {
      setError('Please enter valid price');
      return;
    }
    if (!image) {
      setError('Please upload an image');
      return;
    }
    setError('');
    dispatch(add(payload));
    // NOTE: For development version I don't want to upload image again and again so not going to reset the form
    // event.target.reset();
  };
  
  const onchange = (event) => {
    const file = event.target.files.item(0);
    const imageURL = URL.createObjectURL(file);    
    setImage(imageURL);
  };
  
  return (
    <>
      <ol className={styles.formGrid}>
	<li>
	  <form className={styles.form} onSubmit={submit}>
	    <input placeholder="Please enter product name" value={name} onChange={(event) => setName(event.target.value)} type="text" name="name" required/>
	    <input placeholder="Please enter price of a product" value={price} onChange={(event) => setPrice(parseInt(event.target.value))} type="number" name="price" required/>
	    <input onChange={onchange} type="file" name="file"  multiple={false} />
	    <label> { error } </label>
	    <button type="submit">Submit</button>	    
	  </form>
	</li>
	<li>
	  <strong>
	    Total items in cart: { count }
	  </strong>
	</li>
      </ol>
    </>
  )
}

export default ProductForm;
