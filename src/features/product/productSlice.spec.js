import reducer, { add, remove, wish, unwish } from './productSlice';

const initialState = {
  list: [{ name: 'Test', id: 'test' }]
};

test('Should have initial state', () => {
  const { list } = reducer(undefined, {});
  expect(list.length).toEqual(0);
});

test('Should add new product to list', () => {
  const product = { name: 'Some Product' };
  const { list } = reducer(undefined, add(product));
  expect(list.length).toEqual(1);
  expect(list.find((item) => item.name.toLowerCase()).name).toEqual(product.name);
});

test('Should remove product from list', () => {
  const product = { name: 'Some Product' };
  const { list } = reducer({list: [product]}, remove(product));
  expect(list.length).toEqual(0);
});

test('Should add product to the card', () => {
  const { list } = reducer(initialState, wish('test'));
  expect(list.at(0).wished).toEqual(true);
});

test('Should remove product from the card', () => {
  const { list } = reducer(initialState, unwish('test'));
  expect(list.at(0).wished).toEqual(false);
});
