import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  list: [],
};

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    add(state, { payload }) {
      const key = payload.name.toLowerCase().trim();
      if (!state.list.find((item) => item.id === key)) {
	state.list.push({
	  ...payload,
	  id: key,
	});	
      }
    },
    remove(state, { payload }) {
      const index = state.list.findIndex((item) => item.id === payload);
      state.list.splice(index, 1);
    },
    wish(state, { payload }) {
      const item = state.list.find((item) => item.id === payload);
      item.wished = true;
    },
    unwish(state, { payload }) {
      const item = state.list.find((item) => item.id === payload);
      item.wished = false;
    },
  },
});

export const selectProducts = (state) => state.products.list;
export const selectCount = (state) => state.products.list.filter((item) => item.wished).length;
export const { add, remove, wish, unwish } = productSlice.actions;

export default productSlice.reducer;
