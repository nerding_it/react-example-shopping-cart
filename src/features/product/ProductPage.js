import React from 'react';

import Form from './ProductForm';
import List from './ProductList';

const ProductPage = () => {
  return (
    <>
      <Form />
      <List />
    </>
  );
}

export default ProductPage;
