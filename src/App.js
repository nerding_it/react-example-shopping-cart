import React from 'react';
import { default as Page } from './features/product/ProductPage';

function App() {
  return (
    <>
      <Page />
    </>
  );
}

export default App;
